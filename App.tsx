import React from 'react'
import { StoreRedux } from './src/Redux/StoreRedux'
import { Provider } from 'react-redux'
import DefaultLayout from './src/DefaultLayout/DefaultLayout'
const App = () => {
  return (
    <Provider store={StoreRedux}>
      <DefaultLayout/>
    </Provider>
  )
}

export default App