import {
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useRef} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {VerificationCodeCss} from './VerificationCodeCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {goBack} from '../../../../Util/navigation';
import {LinearColor} from '../../../../Util/styles';
const VerificationCode = ({route}: any) => {
  const {numberPhone} = route.params;
  const FirstInput = useRef<TextInput>(null);
  const SecondInput = useRef<TextInput>(null);
  const thirdInput = useRef<TextInput>(null);
  const fourthInput = useRef<TextInput>(null);
  const handleGoBack = () => {
    goBack();
  };
  return (
    <SafeAreaView style={VerificationCodeCss.Container}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        colors={LinearColor}
        style={{flex: 1, padding: 10}}>
        <ScrollView style={{flex: 1}}>
          <View style={VerificationCodeCss.Header}>
            <View style={VerificationCodeCss.HeaderNav}>
              <TouchableOpacity onPress={handleGoBack}>
                <Ionicons name="chevron-back-outline" size={40} color="#fff" />
              </TouchableOpacity>
              <Text style={VerificationCodeCss.HeaderNavText}>
                Verification
              </Text>
            </View>
            <View style={VerificationCodeCss.HeaderBody}>
              <View style={VerificationCodeCss.HeaderBodyNotification}>
                <Text style={VerificationCodeCss.TextName}>Verification</Text>
                <Text style={VerificationCodeCss.TextNameInfo}>
                  Enter The OTP Code From the Phonewe Just Sent You At + {numberPhone}
                </Text>
              </View>
              <View style={VerificationCodeCss.HeaderForm}>
                <View style={VerificationCodeCss.FormInputVerification}>
                  <View style={VerificationCodeCss.HeaderFormInput}>
                    <TextInput
                      ref={FirstInput}
                      placeholderTextColor="#fff"
                      style={VerificationCodeCss.InputForm}
                      keyboardType="numeric"
                      maxLength={1}
                      onChangeText={text =>
                        text && SecondInput.current?.focus()
                      }
                    />
                  </View>
                  <View style={VerificationCodeCss.HeaderFormInput}>
                    <TextInput
                      ref={SecondInput}
                      placeholderTextColor="#fff"
                      style={VerificationCodeCss.InputForm}
                      keyboardType="numeric"
                      maxLength={1}
                      onChangeText={text =>
                        text
                          ? thirdInput.current?.focus()
                          : FirstInput.current?.focus()
                      }
                    />
                  </View>
                  <View style={VerificationCodeCss.HeaderFormInput}>
                    <TextInput
                      ref={thirdInput}
                      placeholderTextColor="#fff"
                      style={VerificationCodeCss.InputForm}
                      keyboardType="numeric"
                      maxLength={1}
                      onChangeText={text =>
                        text
                          ? fourthInput.current?.focus()
                          : SecondInput.current?.focus()
                      }
                    />
                  </View>
                  <View style={VerificationCodeCss.HeaderFormInput}>
                    <TextInput
                      ref={fourthInput}
                      placeholderTextColor="#fff"
                      style={VerificationCodeCss.InputForm}
                      keyboardType="numeric"
                      maxLength={1}
                      onChangeText={text =>
                        !text && thirdInput.current?.focus()
                      }
                    />
                  </View>
                </View>
                <View style={VerificationCodeCss.ResendCode}>
                  <Text style={VerificationCodeCss.ResendCodeText}>
                    Didn't receive SMS?{' '}
                  </Text>
                  <TouchableOpacity style={VerificationCodeCss.ResendCodeBtn}>
                    <Text style={VerificationCodeCss.ResendCodeBtnText}>
                      Resend Code
                    </Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity style={VerificationCodeCss.BtnPress}>
                  <Text style={VerificationCodeCss.TextBtnPress}>Verify</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default VerificationCode;
