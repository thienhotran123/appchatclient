import { StyleSheet } from 'react-native'
import { scale, verticalScale } from '../../../../Util/reponsive'
import { shadowStyle } from '../../../../Util/styles'

export const VerificationCodeCss = StyleSheet.create({
    Container: {
        flex: 1,
      },
      Header: {
        flex: 1,
      },
      HeaderNav: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
      },
      HeaderNavText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '600',
        paddingLeft: 10,
      },
      HeaderBody: {
        flex: 1,
      },
      HeaderBodyNotification: {
        width: '100%',
        marginTop: 20,
      },
      TextName: {
        fontSize: 30,
        fontWeight: '600',
        color: '#fff',
        paddingVertical: 20,
      },
      TextNameInfo: {
        fontWeight: '600',
        color: '#fff',
        fontSize: 16,
        opacity: 0.68,
      },
      HeaderForm: {
        width: '100%',
        marginTop: 20,
      },
      FormInputVerification:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
      },
      HeaderFormInput: {
        marginHorizontal: 10,
        flexDirection: 'row',
        width: scale(60),
        height: scale(60),
        borderColor: '#EEEDF4',
        backgroundColor: '#C7BAFD',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 10,
        borderWidth: 2,
        marginVertical: 6,
        ...shadowStyle,
      },
      InputForm: {
        flex:1,
        textAlign:'center',
        fontSize:20,
        justifyContent:'center',
        alignItems:'center',
        color: '#fff',
      },
      ResendCode:{
        marginTop:20,
        flexDirection:'row',
        alignItems:'center'
      },
      ResendCodeText:{
        fontSize:18,
        fontWeight:'600',
        color:'#fff',
        opacity:0.68
      },
      ResendCodeBtn:{

      },
      ResendCodeBtnText:{
        fontSize:18,
        fontWeight:'600',
        color:'#009900',
      },
      BtnPress: {
        marginTop:20,
        width: '100%',
        height: verticalScale(60),
        justifyContent: 'center',
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: '#fff',
      },
      TextBtnPress: {color: '#333', fontWeight: '600', fontSize: 18},
      footerContainer: {
        width:'100%',
        justifyContent:'center',
        alignContent:'center',
        flexDirection:'row'
      },
      footerText:{
        color:'#fff',
        fontWeight:'600',
        fontSize:16,
      }
})