import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import {object, string} from 'yup';
import {RegisterCss} from './RegisterCss';
import {SafeAreaView} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {goBack, replace} from '../../../../Util/navigation';
import {Formik} from 'formik';
import {CreateUser} from '../../../../Util/GetApi';
import LoadingApp from '../../../../Components/LoadingComponent/LoadingApp/LoadingApp';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../../Redux/StoreRedux';
import {
  SetLoadingState,
  SetModalIsNotification,
} from '../../../../Redux/StateSlice';
import ModalIsNotification from '../../../../Components/ComponentApp/ModalIsNotification/ModalIsNotification';
import {LinearColor} from '../../../../Util/styles';
const RegisterSchema = object().shape({
  phone: string()
    .max(11, 'Must not exceed 40 characters')
    .required('Please enter your phone')
    .matches(
      /(02|03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
      'Phone number is not in the correct format',
    ),
  username: string()
    .max(40, 'Must not exceed 40 characters')
    .required('Please enter your username'),
  passWord: string()
    .min(4, 'No less than 4 characters')
    .required('Please enter your password'),
  email: string()
    .email('The field must be email')
    .min(4, 'No less than 4 characters')
    .required('Please enter your email'),
});
const Register = () => {
  const dispatch = useDispatch();
  const Loading = useSelector(
    (state: RootState) => state.StateSlice.LoadingState,
  );
  const ShowModalState = useSelector(
    (state: RootState) => state.StateSlice.ShowModalState,
  );
  const [SecureTextEntryPass, SetSecureTextEntry] = useState(true);
  const handleLoginClick = () => {
    goBack();
  };
  const handleSubmitForm = (values: {
    username: string;
    passWord: string;
    email: string;
    phone: string;
  }) => {
    dispatch(SetLoadingState({isLoading: true}));
    CreateUser({
      phone: values.phone,
      username: values.username,
      email: values.email,
      password: values.passWord,
      isAvatarImageSet: false,
      avatarImage: 't23ks23lc0210c0c21d2a',
    })
      .then(data => {
        if (data.status_code == 200) {
          dispatch(
            SetModalIsNotification({
              title: data.message,
              CancelBtn: 'handleCancel',
              handleBtn: '',
              isShow: true,
              icon: 'success',
            }),
          );
          goBack();
        } else {
          dispatch(
            SetModalIsNotification({
              title: data.message,
              CancelBtn: 'handleCancel',
              handleBtn: '',
              isShow: true,
              icon: 'fail',
            }),
          );
        }
      })
      .catch(err => {
        dispatch(
          SetModalIsNotification({
            title: 'check network connection',
            CancelBtn: 'handleCancel',
            handleBtn: '',
            isShow: true,
            icon: 'fail',
          }),
        );
      })
      .finally(() => dispatch(SetLoadingState({isLoading: false})));
  };
  const handlePressPassword = () => {
    SetSecureTextEntry(!SecureTextEntryPass);
  };
  return (
    <SafeAreaView style={RegisterCss.Container}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        colors={LinearColor}
        style={{flex: 1, padding: 10}}>
        <ScrollView
          style={{flex: 1}}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}>
          <View style={RegisterCss.LoginContainer}>
            <Formik
              initialValues={{
                phone: '',
                username: '',
                passWord: '',
                email: '',
              }}
              validationSchema={RegisterSchema}
              onSubmit={values => handleSubmitForm(values)}>
              {({
                handleChange,
                handleSubmit,
                values,
                errors,
                touched,
                isValid,
                setFieldTouched,
              }) => (
                <View style={RegisterCss.HeaderContainer}>
                  <View style={RegisterCss.HeaderTextInfo}>
                    <Text style={RegisterCss.HeaderText}>Create Account,</Text>
                    <Text style={RegisterCss.HeaderText}>
                      to get started now!,
                    </Text>
                  </View>
                  <View style={RegisterCss.HeaderForm}>
                    <View style={RegisterCss.HeaderInput}>
                      <Ionicons name="call-outline" size={22} color="#7B7D82" />
                      <TextInput
                        style={RegisterCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your phone"
                        onChangeText={handleChange('phone')}
                        onBlur={() => setFieldTouched('phone')}
                        value={values.phone}
                        keyboardType="numeric"
                      />
                      {!errors.phone && touched.phone ? (
                        <Ionicons
                          name="checkmark-done-outline"
                          size={22}
                          color="#00CC00"
                        />
                      ) : (
                        ''
                      )}
                    </View>
                    {errors.phone && touched.phone && (
                      <Text style={RegisterCss.ErrorInput}>{errors.phone}</Text>
                    )}
                    <View style={RegisterCss.HeaderInput}>
                      <Ionicons
                        name="person-outline"
                        size={22}
                        color="#7B7D82"
                      />
                      <TextInput
                        style={RegisterCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your username"
                        onChangeText={handleChange('username')}
                        onBlur={() => setFieldTouched('username')}
                        value={values.username}
                      />
                        {!errors.username && touched.username ? (
                        <Ionicons
                          name="checkmark-done-outline"
                          size={22}
                          color="#00CC00"
                        />
                      ) : (
                        ''
                      )}
                    </View>
                    {errors.username && touched.username && (
                      <Text style={RegisterCss.ErrorInput}>
                        {errors.username}
                      </Text>
                    )}
                    <View style={RegisterCss.HeaderInput}>
                      <Ionicons
                        name="lock-closed-outline"
                        size={22}
                        color="#7B7D82"
                      />
                      <TextInput
                        style={RegisterCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your password"
                        onChangeText={handleChange('passWord')}
                        onBlur={() => setFieldTouched('passWord')}
                        value={values.passWord}
                        secureTextEntry={SecureTextEntryPass}
                      />
                      {SecureTextEntryPass ? (
                        <TouchableOpacity onPress={handlePressPassword}>
                          <Ionicons name="eye" size={22} color="#7B7D82" />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity onPress={handlePressPassword}>
                          <Ionicons name="eye-off" size={22} color="#7B7D82" />
                        </TouchableOpacity>
                      )}
                    </View>
                    {errors.passWord && touched.passWord && (
                      <Text style={RegisterCss.ErrorInput}>
                        {errors.passWord}
                      </Text>
                    )}
                    <View style={RegisterCss.HeaderInput}>
                      <Ionicons
                        name="navigate-outline"
                        size={22}
                        color="#7B7D82"
                      />
                      <TextInput
                        style={RegisterCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your email address"
                        onChangeText={handleChange('email')}
                        onBlur={() => setFieldTouched('email')}
                        value={values.email}
                      />
                        {!errors.email && touched.email ? (
                        <Ionicons
                          name="checkmark-done-outline"
                          size={22}
                          color="#00CC00"
                        />
                      ) : (
                        ''
                      )}
                    </View>
                    {errors.email && touched.email && (
                      <Text style={RegisterCss.ErrorInput}>{errors.email}</Text>
                    )}
                    <TouchableOpacity
                      disabled={isValid ? false : true}
                      onPress={() => handleSubmit()}
                      style={
                        isValid
                          ? RegisterCss.BtnSign
                          : RegisterCss.BtnSignNotValid
                      }>
                      <Text style={RegisterCss.BtnLoginTextCss}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={RegisterCss.AlanContainer}>
                      <View style={RegisterCss.Alan}></View>
                      <Text style={RegisterCss.TextAlan}>
                        Or , Sign Up with
                      </Text>
                      <View style={RegisterCss.Alan}></View>
                    </View>
                    <View style={RegisterCss.SignWithBtnContainer}>
                      <TouchableOpacity style={RegisterCss.SignUpWithBtn}>
                        <Image
                          source={require('../../../../Public/image/facebook.jpg')}
                          style={RegisterCss.imageBtn}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity style={RegisterCss.SignUpWithBtn}>
                        <Image
                          source={require('../../../../Public/image/google.jpg')}
                          style={RegisterCss.imageBtn}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )}
            </Formik>
            <View style={RegisterCss.footerContainer}>
              <Text style={RegisterCss.footerText}>
                Already have an account?{' '}
              </Text>
              <TouchableOpacity onPress={handleLoginClick}>
                <Text style={RegisterCss.footerText}>Login Now</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {Loading ? <LoadingApp /> : ''}
        {ShowModalState ? <ModalIsNotification /> : ''}
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Register;
