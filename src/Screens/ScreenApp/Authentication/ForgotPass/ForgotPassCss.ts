import {StyleSheet} from 'react-native';
import {verticalScale} from '../../../../Util/reponsive';
import {shadowStyle} from '../../../../Util/styles';
export const ForgotPassCss = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Header: {
    flex: 1,
  },
  HeaderNav: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  HeaderNavText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600',
    paddingLeft: 10,
  },
  HeaderBody: {
    flex: 1,
  },
  HeaderBodyNotification: {
    width: '100%',
    marginTop: 20,
  },
  TextName: {
    fontSize: 30,
    fontWeight: '600',
    color: '#fff',
    paddingVertical: 20,
  },
  TextNameInfo: {
    fontWeight: '600',
    color: '#fff',
    fontSize: 16,
    opacity: 0.68,
  },
  HeaderForm: {
    width: '100%',
    marginTop: 20,
  },
  HeaderFormInput: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '100%',
    height: verticalScale(60),
    borderColor: '#EEEDF4',
    backgroundColor: '#C7BAFD',
    borderRadius: 10,
    borderWidth: 2,
    marginVertical: 6,
    alignItems: 'center',
    ...shadowStyle,
  },
  InputForm: {
    flex: 1,
    color: '#fff',
  },
  BtnPress: {
    marginTop: 20,
    width: '100%',
    height: verticalScale(60),
    justifyContent: 'center',
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  BtnPressNot:{
    marginTop: 20,
    width: '100%',
    height: verticalScale(60),
    justifyContent: 'center',
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#DDD',
  },
  TextBtnPress: {color: '#333', fontWeight: '600', fontSize: 18},
  footerContainer: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'row',
  },
  footerText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 16,
  },
  ErrorInput: {
    paddingHorizontal: 10,
    color: 'red',
    fontSize: 16,
  },
});
