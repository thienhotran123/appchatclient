import {
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import {ForgotPassCss} from './ForgotPassCss';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {object, string} from 'yup';
import {goBack, navigate, replace} from '../../../../Util/navigation';
import {LinearColor} from '../../../../Util/styles';
import {Formik} from 'formik';
const LoginSchema = object().shape({
  phone: string()
    .max(11, 'Must not exceed 40 characters')
    .required('Please enter your phone')
    .matches(
      /(02|03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
      'Phone number is not in the correct format',
    ),
});
const ForgotPass = () => {
  const handleGoBack = () => {
    goBack();
  };
  const handleSubmitForm = (values: {phone: string}) => {
    navigate('VerificationCode', {numberPhone: values.phone});
  };
  return (
    <SafeAreaView style={ForgotPassCss.Container}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        colors={LinearColor}
        style={{flex: 1, padding: 10}}>
        <ScrollView style={{flex: 1}}>
          <Formik
            initialValues={{
              phone: '',
            }}
            validationSchema={LoginSchema}
            onSubmit={values => handleSubmitForm(values)}>
            {({
              handleChange,
              handleSubmit,
              values,
              errors,
              touched,
              isValid,
              setFieldTouched,
            }) => (
              <View style={ForgotPassCss.Header}>
                <View style={ForgotPassCss.HeaderNav}>
                  <TouchableOpacity onPress={handleGoBack}>
                    <Ionicons
                      name="chevron-back-outline"
                      size={40}
                      color="#fff"
                    />
                  </TouchableOpacity>
                  <Text style={ForgotPassCss.HeaderNavText}>
                    Forgot Password
                  </Text>
                </View>
                <View style={ForgotPassCss.HeaderBody}>
                  <View style={ForgotPassCss.HeaderBodyNotification}>
                    <Text style={ForgotPassCss.TextName}>Forgot PassWord</Text>
                    <Text style={ForgotPassCss.TextNameInfo}>
                      Please Enter Your NumberPhone So We Can Help Recover Your
                      PassWord
                    </Text>
                  </View>
                  <View style={ForgotPassCss.HeaderForm}>
                    <View style={ForgotPassCss.HeaderFormInput}>
                      <TextInput
                        placeholder="Enter Your NumberPhone"
                        placeholderTextColor="#fff"
                        style={ForgotPassCss.InputForm}
                        onChangeText={handleChange('phone')}
                        onBlur={() => setFieldTouched('phone')}
                        value={values.phone}
                        keyboardType="numeric"
                      />
                    </View>
                    {errors.phone && touched.phone && (
                      <Text style={ForgotPassCss.ErrorInput}>
                        {errors.phone}
                      </Text>
                    )}
                    <TouchableOpacity
                      disabled={!isValid ? true : false}
                      onPress={handleSubmit}
                      style={!isValid  ?ForgotPassCss.BtnPressNot : ForgotPassCss.BtnPress}>
                      <Text style={ForgotPassCss.TextBtnPress}>Continue</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}
          </Formik>
        </ScrollView>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default ForgotPass;
