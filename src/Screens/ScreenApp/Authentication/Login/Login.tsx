import {
  SafeAreaView,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useContext, useState} from 'react';
import {LoginCss} from './LoginCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import {navigate, replace} from '../../../../Util/navigation';
import {object, string} from 'yup';
import {Formik} from 'formik';
import {LoginUser} from '../../../../Util/GetApi';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../../Redux/StoreRedux';
import LoadingApp from '../../../../Components/LoadingComponent/LoadingApp/LoadingApp';
import ModalIsNotification from '../../../../Components/ComponentApp/ModalIsNotification/ModalIsNotification';
import {
  SetLoadingState,
  SetModalIsNotification,
} from '../../../../Redux/StateSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LinearColor} from '../../../../Util/styles';
import {Auth} from '../../../../DefaultLayout/DefaultLayout';
import {ScrollView} from 'react-native';
const LoginSchema = object().shape({
  phone: string()
    .max(11, 'Must not exceed 40 characters')
    .required('Please enter your phone')
    .matches(
      /(02|03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
      'Phone number is not in the correct format',
    ),
  password: string()
    .min(4, 'No less than 4 characters')
    .required('Please enter your password'),
});
const Login = () => {
  const dispatch = useDispatch();
  const {Login} = useContext(Auth);
  const Loading = useSelector(
    (state: RootState) => state.StateSlice.LoadingState,
  );
  const ShowModalState = useSelector(
    (state: RootState) => state.StateSlice.ShowModalState,
  );
  const [SecureTextEntryPass, SetSecureTextEntry] = useState<boolean>(true);
  const handleSignUpClick = () => {
    navigate('Register',{});
  };
  const handleForgotClick = () => {
    navigate('ForgotPass', {});
  };
  const handleSubmitForm = async (values: any) => {
    dispatch(SetLoadingState({isLoading: true}));
    LoginUser({phone: values.phone, password: values.password})
      .then(data => {
        if (data.status_code == 200) {
          AsyncStorage.setItem('Login', JSON.stringify(data.responsive))
            .then(() => {
              Login(data.responsive);
            })
            .catch(() =>
              dispatch(
                SetModalIsNotification({
                  title: 'An error occurred',
                  CancelBtn: 'handleCancel',
                  handleBtn: '',
                  isShow: true,
                  icon: 'fail',
                }),
              ),
            );
        } else {
          dispatch(
            SetModalIsNotification({
              title: data.message,
              CancelBtn: 'handleCancel',
              handleBtn: '',
              isShow: true,
              icon: 'fail',
            }),
          );
        }
      })
      .catch(err => {
        dispatch(
          SetModalIsNotification({
            title: 'check network connection',
            CancelBtn: 'handleCancel',
            handleBtn: '',
            isShow: true,
            icon: 'fail',
          }),
        );
      })
      .finally(() => dispatch(SetLoadingState({isLoading: false})));
  };
  const handleSetSecureTextEntry = () => {
    SetSecureTextEntry(!SecureTextEntryPass);
  };
  return (
    <SafeAreaView style={LoginCss.Container}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        colors={LinearColor}
        style={{flex: 1, padding: 10}}>
        <ScrollView
          style={{flex: 1}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={LoginCss.LoginContainer}>
            <View style={LoginCss.HeaderContainer}>
              <View style={LoginCss.HeaderTextInfo}>
                <Text style={LoginCss.HeaderText}>Welcome,</Text>
                <Text style={LoginCss.HeaderText}>Glad to see you!,</Text>
              </View>
              <Formik
                initialValues={{
                  phone: '',
                  password: '',
                }}
                validationSchema={LoginSchema}
                onSubmit={values => handleSubmitForm(values)}>
                {({
                  handleChange,
                  handleSubmit,
                  values,
                  errors,
                  touched,
                  isValid,
                  setFieldTouched,
                }) => (
                  <View style={LoginCss.HeaderForm}>
                    <View style={LoginCss.HeaderInput}>
                      <Ionicons name="call-outline" size={22} color="#7B7D82" />
                      <TextInput
                        style={LoginCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your phone"
                        onChangeText={handleChange('phone')}
                        onBlur={() => setFieldTouched('phone')}
                        value={values.phone}
                        keyboardType="numeric"
                      />
                      {!errors.phone && touched.phone ? (
                        <Ionicons
                          name="checkmark-done-outline"
                          size={22}
                          color="#00CC00"
                        />
                      ) : (
                        ''
                      )}
                    </View>
                    {errors.phone && touched.phone && (
                      <Text style={LoginCss.ErrorInput}>{errors.phone}</Text>
                    )}
                    <View style={LoginCss.HeaderInput}>
                      <Ionicons
                        name="lock-closed-outline"
                        size={22}
                        color="#7B7D82"
                      />
                      <TextInput
                        style={LoginCss.InputCss}
                        placeholderTextColor="#fff"
                        placeholder="Enter your password"
                        onChangeText={handleChange('password')}
                        onBlur={() => setFieldTouched('password')}
                        value={values.password}
                        keyboardType="default"
                        secureTextEntry={SecureTextEntryPass}
                      />
                      {SecureTextEntryPass ? (
                        <TouchableOpacity onPress={handleSetSecureTextEntry}>
                          <Ionicons name="eye" size={22} color="#7B7D82" />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity onPress={handleSetSecureTextEntry}>
                          <Ionicons name="eye-off" size={22} color="#7B7D82" />
                        </TouchableOpacity>
                      )}
                    </View>
                    {errors.password && touched.password && (
                      <Text style={LoginCss.ErrorInput}>{errors.password}</Text>
                    )}
                    <TouchableOpacity
                      onPress={handleForgotClick}
                      style={LoginCss.TextForgot}>
                      <Text style={LoginCss.TextForgotCss}>
                        Forgot Password
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={handleSubmit}
                      disabled={isValid ? false : true}
                      style={
                        isValid ? LoginCss.BtnLogin : LoginCss.BtnLoginValid
                      }>
                      <Text style={LoginCss.BtnLoginTextCss}>Login</Text>
                    </TouchableOpacity>
                    <View style={LoginCss.AlanContainer}>
                      <View style={LoginCss.Alan}></View>
                      <Text style={LoginCss.TextAlan}>Or , Login with</Text>
                      <View style={LoginCss.Alan}></View>
                    </View>
                    <View style={LoginCss.LoginWithBtnContainer}>
                      <TouchableOpacity style={LoginCss.LoginWithBtn}>
                        <Image
                          source={require('../../../../Public/image/facebook.jpg')}
                          style={LoginCss.imageBtn}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity style={LoginCss.LoginWithBtn}>
                        <Image
                          source={require('../../../../Public/image/google.jpg')}
                          style={LoginCss.imageBtn}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </Formik>
            </View>
            <View style={LoginCss.footerContainer}>
              <Text style={LoginCss.footerText}>Don't have an account? </Text>
              <TouchableOpacity onPress={handleSignUpClick}>
                <Text style={LoginCss.footerText}>Sign Up Now</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {Loading ? <LoadingApp /> : ''}
        {ShowModalState ? <ModalIsNotification /> : ''}
      </LinearGradient>
    </SafeAreaView>
  );
};

export default Login;
