import {StyleSheet} from 'react-native';
import { scale, verticalScale} from '../../../../Util/reponsive';
import  { shadowStyle } from '../../../../Util/styles';
export const LoginCss = StyleSheet.create({
  Container: {
    flex: 1,
  },
  LoginContainer: {
    flex:1,
    padding:10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  HeaderContainer: {
    flex:1,
  },
  HeaderTextInfo: {
    paddingVertical:20,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HeaderText: {
    color: '#FBF9FF',
    fontWeight: '600',
    fontSize: 32,
  },
  HeaderForm: {
    flex:1
  },
  HeaderInput: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '100%',
    height: verticalScale(60),
    borderColor: '#EEEDF4',
    backgroundColor: '#C7BAFD',
    borderRadius: 10,
    borderWidth: 2,
    marginVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
    ...shadowStyle
  },
  InputCss: {
    flex: 1,
    color: '#fff',
    height: verticalScale(60),
    paddingHorizontal: 10,
  },
  TextForgot: {
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingVertical: 10,
  },
  TextForgotCss: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
  },
  BtnLogin: {
    marginVertical: 20,
    width: '100%',
    height: verticalScale(66),
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    ...shadowStyle
  },
  BtnLoginValid:{
    marginVertical: 20,
    width: '100%',
    height: verticalScale(66),
    backgroundColor: '#DDDDDD',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    ...shadowStyle
  },
  BtnLoginTextCss: {
    color: '#181818',
    fontWeight: '600',
    fontSize: 18,
  },
  AlanContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Alan: {
    borderColor: '#fff',
    borderWidth: 0.8,
    flex: 1,
  },
  TextAlan: {
    color: '#fff',
    paddingHorizontal: 10,
    fontSize: 16,
    fontWeight: '600',
  },
  LoginWithBtnContainer: {
    marginVertical:30,
    width: '100%',
    height: verticalScale(66),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  LoginWithBtn: {
    width: '48%',
    height: '100%',
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent:'center',
    alignItems:'center' ,
    ...shadowStyle
  },
  imageBtn: {
    width: scale(34),
    height: scale(34),
  },
  footerContainer: {
    width:'100%',
    justifyContent:'center',
    alignContent:'center',
    flexDirection:'row'
  },
  footerText:{
    color:'#fff',
    fontWeight:'600',
    fontSize:16,
  },
  ErrorInput: {
    paddingHorizontal:10,
    color:'red',
    fontSize:16
  },
});
