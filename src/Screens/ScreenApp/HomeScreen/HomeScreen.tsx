import {
  SafeAreaView,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useRef} from 'react';
import {HomeCss} from './HomeCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ListUser from '../../../Components/ComponentApp/ListUser/ListUser';
import {io} from 'socket.io-client';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../Redux/StoreRedux';
import {ListUserOnline} from '../../../Components/Components';
import {getAllUsers} from '../../../Util/GetApi';
import {SetListFriends} from '../../../Redux/StateSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
const HomeScreen = () => {
  const dispatch = useDispatch();
  const SocketRef = useRef<any>(null);
  const user = useSelector((state: RootState) => state.StateSlice.user);
  const listFriends = useSelector(
    (state: RootState) => state.StateSlice.listFriends,
  );
  useEffect(() => {
    if (user.id !== '') {
      SocketRef.current = io('http://192.168.174.1:3000');
      SocketRef.current.emit('ConnectUser', {
        id: user.id,
        username: user.name,
        email: user.email,
      });
    }
  }, [user]);
  useEffect(() => {
    getAllUsers()
      .then(data => {
        const Friends = data.responsive.map((item: any) => {
          if (item._id == user.id) {
            return {
              username: 'YourSelf',
              id: item._id,
              avatarImage: item.avatarImage,
              email: item.email,
            };
          } else {
            return {
              username: item.username,
              id: item._id,
              avatarImage: item.avatarImage,
              email: item.email,
            };
          }
        });
        dispatch(SetListFriends(Friends));
      })
      .catch(err => console.log(err));
  }, []);
  useEffect(() => {}, []);
  const handelShowDrawer = () => {
    AsyncStorage.removeItem('Login');
  };
  return (
    <SafeAreaView style={HomeCss.Container}>
      <View style={HomeCss.ContainerHome}>
        <View style={HomeCss.HeaderContainer}>
          <View style={HomeCss.Header}>
            <View style={HomeCss.HeaderInfo}>
              <View style={HomeCss.HeaderImage}>
                <TouchableOpacity onPress={handelShowDrawer}>
                  <Ionicons
                    name="reorder-three-outline"
                    size={30}
                    style={{
                      backgroundColor: '#EEEEEE',
                      borderRadius: 50,
                      padding: 4,
                    }}
                  />
                  <View style={HomeCss.notificationTotal}>
                    <Text style={HomeCss.notificationTotalText}>1</Text>
                  </View>
                </TouchableOpacity>
                <View style={HomeCss.TextContainer}>
                  <Text style={HomeCss.TextName} numberOfLines={1}>
                    Đoạn chat
                  </Text>
                </View>
              </View>
              <View style={HomeCss.HeaderIcon}>
                <TouchableOpacity style={{marginRight: 10}}>
                  <Ionicons name="notifications" size={30} color="#BBBBBB" />
                  <View style={HomeCss.notificationTotal}>
                    <Text style={HomeCss.notificationTotalText}>1</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={HomeCss.HeaderSearch}>
              <View style={HomeCss.IconInputCss}>
                <Ionicons name="search" size={20} color="#AAAAAA" />
              </View>
              <TextInput
                style={HomeCss.inputSearch}
                placeholder="Search"
                placeholderTextColor="#AAAAAA"
              />
            </View>
            <View style={HomeCss.HeaderUserOnline}>
              <ScrollView
                style={HomeCss.ScrollUserOnline}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                horizontal={true}>
                <View style={HomeCss.ListUserContainer}>
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                  <ListUserOnline />
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
        <View style={HomeCss.Body}>
          <ScrollView
            style={HomeCss.ScrollBodyContainer}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {listFriends.map((friend, index) => (
              <ListUser friendItem={friend} SocketRef={SocketRef} key={index} />
            ))}
          </ScrollView>
        </View>
        <View style={HomeCss.Footer}></View>
      </View>
    </SafeAreaView>
  );
};

export default React.memo(HomeScreen);
