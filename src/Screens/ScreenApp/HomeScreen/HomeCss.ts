import {StyleSheet} from 'react-native';
import {deviceWidth, scale, verticalScale} from '../../../Util/reponsive';
export const HomeCss = StyleSheet.create({
  Container: {
    flex: 1,
    padding:10,
    backgroundColor: '#fff',
  },
  ContainerHome: {
    flex: 1,
  },
  HeaderContainer: {
    width: '100%',
  },
  HeaderInfo: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  HeaderSearch: {
    paddingHorizontal: 10,
    marginTop: 10,
    width: '100%',
    height: verticalScale(52),
    borderRadius: 50,
    backgroundColor: '#EEEEEE',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputSearch: {
    flex: 1,
    height: verticalScale(52),
  },
  Header: {},
  HeaderImage: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  TextContainer: {},
  TextName: {
    paddingLeft: 10,
    fontWeight: '600',
    fontSize: deviceWidth * 0.054,
    color: '#333',
  },
  notificationTotal: {
    position: 'absolute',
    width: 20,
    height: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    right: -6,
    top: -8,
  },
  notificationTotalText:{
    color: '#fff',
    fontWeight: '600',
    fontSize: 12
  },
  TextStatus: {
    paddingLeft: 10,
    fontWeight: '600',
    fontSize: deviceWidth * 0.042,
    color: '#333',
  },
  IconInputCss: {
    paddingRight: 6,
  },
  HeaderIcon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  HeaderUserOnline: {
    width: '100%',
  },
  ScrollUserOnline: {
    width: '100%',
    marginTop: 16,
  },
  ListUserItemContainer: {
    width: scale(80),
    height: scale(80),
    flexDirection: 'column',
    alignItems: 'center',
  },
  ListUserContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  ListUserItem: {
    borderWidth: 3,
    borderColor: '#56AEF2',
    padding: 4,
    borderRadius: 50,
    marginRight: 10,
    width: scale(50),
    height: scale(50),
  },
  onLinePoint: {
    width: 16,
    height: 16,
    borderRadius: 50,
    position: 'absolute',
    bottom: -2,
    right: -2,
    backgroundColor: '#33CC33',
  },
  Body: {
    flex: 1,
    marginTop: 10,
  },
  ScrollBodyContainer: {
    flex: 1,
  },
  BodyListUser: {
    marginVertical: 4,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: verticalScale(80),
    borderRadius: 8,
  },
  BodyImageCss: {
    width: scale(48),
    height: scale(48),
    borderRadius: 50,
    marginRight: 10,
  },
  BodyUserInfo: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  BodyUserTextInfo: {
    flexDirection: 'column',
    flex: 1,
  },
  BodyUserTextInfoName: {
    fontWeight: '700',
    fontSize: deviceWidth * 0.048,
    color: '#333',
  },
  BodyUserTextInfoMessage: {
    fontWeight: '700',
    fontSize: deviceWidth * 0.038,
    color: '#AAAAAA',
  },
  Footer: {},
});
