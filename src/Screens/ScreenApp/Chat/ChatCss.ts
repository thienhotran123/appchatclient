import {StyleSheet} from 'react-native';
import {scale} from '../../../Util/reponsive';
export const ChatCss = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  ContainerScreen: {
    flex: 1,
    backgroundColor: '#fff',
  },
  ContainerHeader: {
    width: '100%',
    borderBottomColor: '#BBBBBB',
    borderBottomWidth: 2,
  },
  ContainerHeaderChild: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HeaderName: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  HeaderInfo: {
    flex: 1,
  },
  HeaderInfoName: {
    color: '#333',
    fontWeight: '600',
    fontSize: 18,
  },
  HeaderInfoStatus: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  HeaderInfoStatusPoint: {
    width: 10,
    height: 10,
    borderRadius: 50,
    backgroundColor: '#00CC33',
    marginRight: 6,
  },
  HeaderInfoStatusText: {
    color: '#00CC33',
    fontWeight: '600',
    fontSize: 14,
  },
  IconCallHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  IconCallHeaderVideoCam: {
    marginRight: 10,
  },
  ContainerBody: {
    flex: 1,
    paddingHorizontal: 10,
  },
  ContainerFooter: {
    width: '100%',
  },
  FooterForm: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  InputChatContainer: {
    backgroundColor: '#DDDDDD',
    height: scale(40),
    borderRadius: 20,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  InputCss: {
    flex: 1,
    paddingHorizontal: 20,
  },
  IconFooter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ChatListContainer: {
    flex: 1,
    paddingBottom: 10,
  },
  ChatListFriend: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  ChatListFriendMessage: {
    backgroundColor: '#0065D7',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
  },
  ChatListFriendMessageText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 16,
  },
  ContainerChatMessageFriend: {
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
  },
  ContainerIconOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ChatListYourSelf: {
    width: '100%',
    marginVertical: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  ContainerChatMessageYourSelf: {
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
  },
  ChatListYourSelfMessage: {
    backgroundColor: '#0065D7',
    padding: 10,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  ChatListYourSelfMessageText: {
    color: '#fff',
    fontWeight: '600',
    fontSize: 16,
  },
  TextEmoji:{
    fontSize:26,
  }
});
