import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  TextInput,
  LogBox,
  ActivityIndicator,
  TextInputState,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import React, {useEffect, useRef, useState} from 'react';
import {ChatCss} from './ChatCss';
import {scale} from '../../../Util/reponsive';
import {goBack} from '../../../Util/navigation';
import {
  ListMessageFriend,
  ListMessageYour,
} from '../../../Components/Components';
import {useSelector} from 'react-redux';
import {RootState} from '../../../Redux/StoreRedux';
import {CreateMessage, getAllMessages} from '../../../Util/GetApi';
import {FlatList} from 'react-native';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const Chat = ({route}: any) => {
  const SocketRef = route.params.SocketRef;
  const user = useSelector((state: RootState) => state.StateSlice.user);
  const friend = route.params.friendItem;
  const InputMessageRef = useRef<TextInput>(null);
  const [TextString, SetTextString] = useState('');
  const [LoadingMessage, SetLoadingCallMessage] = useState<boolean>(false);
  const [isScrolledToEnd, setIsScrolledToEnd] = useState(false);
  const [ShowTouchOption, SetShowTouchOption] = useState<boolean>(true);
  const [MessageChat, setMessageChat] = useState<
    {fromSelf: boolean; message: string}[]
  >([]);
  const [arrivalMessage, setArrivalMessage] = useState<any>(null);
  const handleNavigate = () => {
    goBack();
  };
  useEffect(() => {
    getAllMessages({from: user.id, to: friend.id, skip: MessageChat.length})
      .then(data => {
        setMessageChat(data.responsive);
      })
      .catch(err => console.error(err));
  }, []);
  useEffect(() => {
    if (SocketRef.current) {
      SocketRef.current.on('messageRecieve', (msg: string) => {
        setArrivalMessage({fromSelf: false, message: msg});
      });
    }
  }, []);
  useEffect(() => {
    arrivalMessage && setMessageChat(prev => [arrivalMessage, ...prev]);
  }, [arrivalMessage]);

  const handleChangeText = (string: string) => {
    if (string.length > 0) {
      SetShowTouchOption(false);
    }
    SetTextString(string);
  };
  // const handleScrollToEnd = () => {
  //   if (isScrolledToEnd) {
  //     if (!LoadingMessage) {
  //       SetLoadingCallMessage(true);
  //       getAllMessages({from: user.id, to: friend.id, skip: MessageChat.length})
  //         .then(data => {
  //           if (data.responsive.length > 0) {
  //             setMessageChat(prev => [...prev, ...data.responsive]);
  //           }
  //         })
  //         .catch(err => console.error(err))
  //         .finally(() => SetLoadingCallMessage(false));
  //     }
  //   }
  // };

  const handleSendMessage = (msg: string) => {
    SetTextString('');
    const msgText = msg !== '' ? msg : TextString;
    if (SocketRef.current) {
      SocketRef.current.emit('message', {
        text: msgText,
        from: user.id,
        to: friend.id,
      });
      const message = [...MessageChat];
      CreateMessage({message: msgText, from: user.id, to: friend.id})
        .then(data => {
          message.unshift({fromSelf: true, message: msgText});
          setMessageChat(message);
        })
        .catch(err => {});
    }
  };

  const renderItem = (item: any) => {
    if (item.item.fromSelf) {
      return <ListMessageYour item={item.item} />;
    } else {
      return <ListMessageFriend item={item.item} />;
    }
  };
  const handleFocus = () => {
    SetShowTouchOption(false);
  };
  const handleBlur = () => {
    SetShowTouchOption(true);
  };
  const handleShowTouchOption = () => {
    SetShowTouchOption(true);
  };
  return (
    <SafeAreaView style={ChatCss.Container}>
      <View style={ChatCss.ContainerScreen}>
        <View style={ChatCss.ContainerHeader}>
          <View style={ChatCss.ContainerHeaderChild}>
            <View style={ChatCss.HeaderName}>
              <TouchableOpacity onPress={handleNavigate}>
                <Ionicons
                  name="chevron-back-outline"
                  size={40}
                  color="#0065D7"
                />
              </TouchableOpacity>
              <Image
                source={require('../../../Public/image/user.jpg')}
                style={{
                  width: scale(40),
                  height: scale(40),
                  borderRadius: 50,
                  marginHorizontal: 10,
                }}
              />
              <View style={ChatCss.HeaderInfo}>
                <Text style={ChatCss.HeaderInfoName}>{friend.username}</Text>
                <View style={ChatCss.HeaderInfoStatus}>
                  <View style={ChatCss.HeaderInfoStatusPoint}></View>
                  <Text style={ChatCss.HeaderInfoStatusText}>Active Now</Text>
                </View>
              </View>
            </View>
            <View style={ChatCss.IconCallHeader}>
              <TouchableOpacity style={{paddingHorizontal: 4}}>
                <Ionicons name="videocam" size={30} color="#0065D7" />
              </TouchableOpacity>
              <TouchableOpacity style={{paddingHorizontal: 4}}>
                <Ionicons name="call" size={30} color="#0065D7" />
              </TouchableOpacity>
              <TouchableOpacity style={{paddingHorizontal: 4}}>
                <Ionicons name="alert-circle" size={30} color="#0065D7" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={ChatCss.ContainerBody}>
          {LoadingMessage ? (
            <ActivityIndicator size="large" color="#0065D7" />
          ) : null}
          <FlatList
            style={{flex: 1}}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            inverted
            data={MessageChat}
            keyExtractor={(item, index) => index.toString()}
            renderItem={item => renderItem(item)}
          />
        </View>
        <View style={ChatCss.ContainerFooter}>
          <View style={ChatCss.FooterForm}>
            {ShowTouchOption ? (
              <>
                <TouchableOpacity>
                  <Ionicons name="add-circle" size={32} color="#0065D7" />
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal: 5}}>
                  <Ionicons name="camera" size={32} color="#0065D7" />
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal: 5}}>
                  <Ionicons name="image-outline" size={32} color="#0065D7" />
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal: 5}}>
                  <Ionicons name="mic" size={28} color="#0065D7" />
                </TouchableOpacity>
              </>
            ) : (
              <TouchableOpacity
                onPress={handleShowTouchOption}
                style={{paddingRight: 5}}>
                <Ionicons name="chevron-forward" size={28} color="#0065D7" />
              </TouchableOpacity>
            )}
            <View style={ChatCss.InputChatContainer}>
              <TextInput
                ref={InputMessageRef}
                onFocus={handleFocus}
                onBlur={handleBlur}
                style={ChatCss.InputCss}
                placeholder="Enter a message"
                value={TextString}
                keyboardType="default"
                onChangeText={e => handleChangeText(e)}
              />
              <TouchableOpacity style={{paddingRight: 10}}>
                <Ionicons name="happy-outline" size={28} color="#0065D7" />
              </TouchableOpacity>
            </View>
            <View style={ChatCss.IconFooter}>
              {TextString.length > 0 ? (
                <TouchableOpacity
                  onPress={() => handleSendMessage('')}
                  style={{paddingHorizontal: 10}}>
                  <Ionicons name="send" size={28} color="#0065D7" />
                </TouchableOpacity>
              ) : (
                <>
                  <TouchableOpacity
                    onPress={() => handleSendMessage('👍')}
                    style={{paddingHorizontal: 10}}>
                    <Text style={ChatCss.TextEmoji}>👍</Text>
                  </TouchableOpacity>
                </>
              )}
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Chat;
