import { configureStore } from "@reduxjs/toolkit";
import StateSlice from "./StateSlice";
export const StoreRedux = configureStore({
    reducer:{
        StateSlice:StateSlice
    }
})

export type RootState = ReturnType<typeof StoreRedux.getState>
export type AppDispatch = typeof StoreRedux.dispatch