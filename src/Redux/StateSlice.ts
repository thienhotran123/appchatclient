import {createSlice} from '@reduxjs/toolkit';
import type {PayloadAction} from '@reduxjs/toolkit';
import {ReduxState} from '../Interface/ReduxStateInterface';
import {ListFriends} from '../Interface/ListFriendsInterFace';
import {Messages} from '../Interface/MessagesInterface';
import {User} from '../Interface/UserInterface';
const initialState: ReduxState = {
  LoadingState: false,
  ShowModalState: false,
  user: {name: '', password: '', image: '', id: '', email : ''},
  messages: {text: ''},
  listFriends: [],
  ModalInfo: {title: '', CancelBtn: '', handleBtn: '', isShow: false, icon: ''},
};
export const StateSlice = createSlice({
  name: 'StateSlice',
  initialState,
  reducers: {
    SetState: (state, actions: PayloadAction<User>) => {
      state.user = actions.payload;
    },
    SetListFriends: (state, actions: PayloadAction<ListFriends[]>) => {
      state.listFriends = actions.payload;
    },
    SetMessages: (state, actions: PayloadAction<Messages>) => {
      state.messages = actions.payload;
    },
    SetLoadingState: (state, actions: PayloadAction<{isLoading: boolean}>) => {
      state.LoadingState = actions.payload.isLoading;
    },
    SetShowModalState: (state, actions: PayloadAction<{isShow: boolean}>) => {
      state.ShowModalState = actions.payload.isShow;
    },
    SetModalIsNotification: (
      state,
      actions: PayloadAction<{
        title: string;
        CancelBtn: string;
        handleBtn: string;
        isShow: boolean;
        icon: string;
      }>,
    ) => {
      (state.ModalInfo = actions.payload),
        (state.ShowModalState = actions.payload.isShow);
    },
  },
});

export const {
  SetState,
  SetListFriends,
  SetShowModalState,
  SetLoadingState,
  SetMessages,
  SetModalIsNotification,
} = StateSlice.actions;
export default StateSlice.reducer;
