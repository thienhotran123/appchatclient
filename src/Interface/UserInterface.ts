interface User {
    name : string , 
    password : string,
    email : string,
    image : string,
    id:string
}

export type {
    User
}