interface ListFriends {
    username?:string ,
    id?:string ,
    avatarImage?:string,
    email?:string,
}

export type {
    ListFriends
}