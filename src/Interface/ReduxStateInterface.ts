import { ListFriends } from './ListFriendsInterFace';
import { Messages } from './MessagesInterface';
import {User} from './UserInterface';

interface ReduxState {
  LoadingState:boolean,
  ShowModalState:boolean,
  user: User;
  messages: Messages;
  listFriends:ListFriends[] ,
  ModalInfo:{title : string , CancelBtn : string , handleBtn :string ,isShow : boolean , icon:string},
}
export type {ReduxState};
