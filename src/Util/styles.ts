const LinearColor = ['#C6A2FF', '#ACA7FF', '#93ACFF', '#8EADFF']
const shadowStyle={
    shadowColor: '#52006A',
    elevation:6
}

export {
    shadowStyle ,
    LinearColor
}