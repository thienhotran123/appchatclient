import {Dimensions, Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
let {width, height} = Dimensions.get('window');
const highDimension = width > height ? width : height;
const lowDimension = width > height ? height : width;
if (DeviceInfo.isTablet()) {
  width = highDimension;
  height = lowDimension;
} else {
  width = lowDimension;
  height = highDimension;
}
const baseIPhone = {
  width: 375,
  height: 812,
};
const baseGooglePix5 = {
  width: 375,
  height: 812,
};
const baseTablet = {
  width: 1024,
  height: 768,
};
const guidelineBaseWidth =Platform.select({ ios: DeviceInfo.isTablet() ? baseTablet.width : baseIPhone.width,android: DeviceInfo.isTablet() ? baseTablet.width - 300: baseGooglePix5.width}) || baseIPhone.width;
const guidelineBaseHeight =
  Platform.select({
    ios: DeviceInfo.isTablet() ? baseTablet.height - 280 : baseIPhone.height,
    android: DeviceInfo.isTablet() ? baseTablet.height - 280: baseGooglePix5.height,
  }) || baseIPhone.height;

const scale = (size: number) => (width / guidelineBaseWidth) * size;
const verticalScale = (size: number) => (height / guidelineBaseHeight) * size ;

const moderateScale = (size: number, factor = 0.5) => {
  return Math.round(scale(size));
};

const deviceWidth = width;
const deviceHeight = height;

export const getPlatform = () => Platform.OS;
export {
  scale,
  verticalScale,
  moderateScale,
  height,
  width,
  deviceWidth,
  deviceHeight,
};