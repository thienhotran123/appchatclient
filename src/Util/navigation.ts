import {
  createNavigationContainerRef,
  StackActions,
} from '@react-navigation/native';
export const navigationRef = createNavigationContainerRef();
export function navigate(name: string, params: any) {
  if (navigationRef.current && navigationRef) {
    navigationRef.current.navigate(name as never, params as never);
  }
}
export function goBack() {
  if (navigationRef.current && navigationRef) {
    navigationRef.current.goBack();
  }
}
export function replace(name: string) {
  if (navigationRef.current && navigationRef) {
    navigationRef.current.dispatch(StackActions.replace(name));
  }
}
