import axios from 'axios';
const host = 'http://192.168.174.1:3000/api';
interface CreateUserData {
  phone: string;
  username: string;
  email: string;
  password: string;
  isAvatarImageSet: boolean;
  avatarImage: string;
}
interface login {
  phone: string;
  password: string;
}
interface GetMessage {
  from: string;
  to: string;
  skip: number;
}
interface CreateMessageData {}
const LoginUser = async (params: login) => {
  const data = await axios({
    method: 'POST',
    url: `${host}/users/LoginUser`,
    timeout: 10000,
    data: params,
  });
  return data.data;
};
const getAllUsers = async () => {
  const data = await axios({
    method: 'GET',
    url: `${host}/users/getUsers`,
    timeout: 10000,
  });
  return data.data;
};

const getAllMessages = async (params: GetMessage) => {
  const data = await axios({
    method: 'POST',
    url: `${host}/messages/getMessages`,
    timeout: 10000,
    data: params,
  });
  return data.data;
};

const CreateUser = async (params: CreateUserData) => {
  const data = await axios({
    method: 'POST',
    url: `${host}/users/CreateUser`,
    timeout: 10000,
    data: params,
  });
  return data.data;
};
const CreateMessage = async (params: CreateMessageData) => {
  const data = await axios({
    method: 'POST',
    url: `${host}/messages/createMessages`,
    timeout: 10000,
    data: params,
  });
  return data.data;
};

export {getAllUsers, getAllMessages, CreateUser, CreateMessage, LoginUser};
