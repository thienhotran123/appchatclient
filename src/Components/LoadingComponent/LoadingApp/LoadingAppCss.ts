import {StyleSheet} from 'react-native';
export const LoadingAppCss = StyleSheet.create({
    LoadingContainer:{
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        top:0,
        backgroundColor:'#333',
        opacity:0.5,
        justifyContent:'center',
        alignItems:'center'
    }
});
