import {View, ActivityIndicator} from 'react-native';
import React from 'react';
import {LoadingAppCss} from './LoadingAppCss';
const LoadingApp = () => {
  return (
    <View style={LoadingAppCss.LoadingContainer}>
      <ActivityIndicator size="large" color="#333" />
    </View>
  );
};

export default LoadingApp;
