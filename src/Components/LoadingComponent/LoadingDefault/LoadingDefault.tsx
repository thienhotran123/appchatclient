import {ActivityIndicator, View} from 'react-native';
import React from 'react';
import { LoadingDefaultCss } from './LoaddingDefaultCss';
const LoadingDefault = () => {
  return (
    <View style={LoadingDefaultCss.LoadingContainer}>
      <ActivityIndicator size={46} color="#ACA7FF" />
    </View>
  );
};

export default LoadingDefault;
