import {StyleSheet} from 'react-native';

export const LoadingDefaultCss = StyleSheet.create({
    LoadingContainer:{
        flex:1,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center'
    }
});
