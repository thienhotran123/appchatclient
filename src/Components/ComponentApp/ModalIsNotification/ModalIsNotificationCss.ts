import {StyleSheet} from 'react-native';
import { scale, verticalScale} from '../../../Util/reponsive';
export const ModalIsNotificationCss = StyleSheet.create({
  Container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    top: 0,
    right: 0,
    backgroundColor: '#333',
    opacity: 0.9,
    zIndex:1,
  },
  ContainerModal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalShow: {
    backgroundColor: '#fff',
    width: '80%',
    height: verticalScale(300),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2,
    opacity:1
  },
  HeaderModal: {
    paddingVertical: 10,
  },
  TextHeader: {
    width: '100%',
    textAlign: 'center',
    fontSize: 20,
    color: '#333',
    fontWeight: '600',
  },
  BodyModal: {
    backgroundColor:'#FFFFF',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  FooterModal: {
    width: '100%',
  },
  FooterModalCustomerText: {
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  FooterModalCustomerCancel: {
    width: '90%',
    height: verticalScale(46),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius: 10,
    marginBottom:10
  },
  FooterModalCustomerTextCancel: {
    width: '100%',
    textAlign: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '600',
  },
  FooterModalCustomerConfirm: {
    width: '90%',
    height: verticalScale(46),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0099FF',
    borderRadius: 10,
    marginBottom:10
  },
  FooterModalCustomerTextConfirm: {
    width: '100%',
    textAlign: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '600',
  },
  StyleImage: {
    width: scale(60),
    height: scale(60),
  },
});
