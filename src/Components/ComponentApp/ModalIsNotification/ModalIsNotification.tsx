import {Image, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {ModalIsNotificationCss} from './ModalIsNotificationCss';
import {useDispatch, useSelector} from 'react-redux';
import {SetModalIsNotification} from '../../../Redux/StateSlice';
import {RootState} from '../../../Redux/StoreRedux';
const ModalIsNotification = () => {
  const dispatch = useDispatch();
  const ModalInfo = useSelector((state: RootState) => state.StateSlice.ModalInfo);
  const handleCancelClick = () => {
    dispatch(
      SetModalIsNotification({
        title: '',
        CancelBtn: '',
        handleBtn: '',
        isShow: false,
        icon: '',
      }),
    );
  };
  return (
    <View style={ModalIsNotificationCss.Container}>
      <View style={ModalIsNotificationCss.ContainerModal}>
        <View style={ModalIsNotificationCss.ModalShow}>
          <View style={ModalIsNotificationCss.HeaderModal}>
            <Text style={ModalIsNotificationCss.TextHeader} numberOfLines={2}>
              {ModalInfo.title}
            </Text>
          </View>
          <View style={ModalIsNotificationCss.BodyModal}>
            <Image
              source={
                ModalInfo.icon == 'fail'
                  ? require('../../../Public/image/fail.png')
                  : require('../../../Public/image/successIcon.png')
              }
              style={ModalIsNotificationCss.StyleImage}
            />
          </View>
          <View style={ModalIsNotificationCss.FooterModal}>
            <View style={ModalIsNotificationCss.FooterModalCustomerText}>
              {ModalInfo.CancelBtn !== '' ? (
                <TouchableOpacity
                  onPress={handleCancelClick}
                  style={ModalIsNotificationCss.FooterModalCustomerCancel}>
                  <Text
                    style={
                      ModalIsNotificationCss.FooterModalCustomerTextCancel
                    }>
                    Cancel
                  </Text>
                </TouchableOpacity>
              ) : (
                ''
              )}
              {ModalInfo.handleBtn !== '' ? (
                <TouchableOpacity
                  style={ModalIsNotificationCss.FooterModalCustomerConfirm}>
                  <Text
                    style={
                      ModalIsNotificationCss.FooterModalCustomerTextConfirm
                    }>
                    Confirm
                  </Text>
                </TouchableOpacity>
              ) : (
                ''
              )}
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default React.memo(ModalIsNotification);
