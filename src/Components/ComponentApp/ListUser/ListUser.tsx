import {Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {ListUserCss} from './ListUserCss';
import {navigate} from '../../../Util/navigation';
const ListUser = ({SocketRef ,friendItem}: any) => {
  const handlePress = () => {
    navigate('Chat', {friendItem: friendItem , SocketRef:SocketRef});
  };
  return (
    <TouchableOpacity onPress={handlePress} style={ListUserCss.BodyListUser}>
      <View style={ListUserCss.BodyUserInfo}>
        <Image
          source={require('../../../Public/image/user.jpg')}
          style={ListUserCss.BodyImageCss}
        />
        <View style={ListUserCss.BodyUserTextInfo}>
          <Text style={ListUserCss.BodyUserTextInfoName} numberOfLines={1}>
            {friendItem.username}
          </Text>
          <Text style={ListUserCss.BodyUserTextInfoMessage} numberOfLines={2}>
            example
          </Text>
        </View>
      </View>
      <View style={ListUserCss.UserNotification}>
        <Ionicons name="checkmark-circle" size={22} color="#999999" />
      </View>
    </TouchableOpacity>
  );
};

export default React.memo(ListUser);
