import { StyleSheet } from 'react-native'
import { deviceWidth, scale, verticalScale } from '../../../Util/reponsive'
export const ListUserCss = StyleSheet.create({
    BodyListUser: {
        marginVertical:4,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: verticalScale(80),
        borderRadius: 8,
      },
      BodyUserInfo:{
        flexDirection:'row',
        flex:1,
        alignItems:'center'
      },
      BodyImageCss: {
        width: scale(52),
        height: scale(52),
        borderRadius: 50,
        marginRight: 10,
      },
      BodyUserTextInfo:{
        flexDirection:'column',
        flex:1,
      },
      BodyUserTextInfoName:{
        fontWeight:'600',
        fontSize: deviceWidth * 0.048,
        color:'#AAAAAA'
      },
      BodyUserTextInfoMessage:{
        fontWeight:'600',
        fontSize: deviceWidth * 0.038,
        color:'#AAAAAA'
      },
      UserNotification:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'center'
      },
      NotSeen:{
        width:20,
        height:20,
        justifyContent: 'center',
        alignItems:'center' ,
        backgroundColor:'#AAAAAA',
        borderRadius:50,
        marginLeft:6
      },
      NotSeenText:{
        color:'#fff',
        fontSize:12 ,
        fontWeight:'600'
      }
})