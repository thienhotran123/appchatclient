import {StyleSheet} from 'react-native';

export const ListMessageFriendCss = StyleSheet.create({
  ChatListFriend: {
    marginVertical: 4,
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  ChatListFriendMessage: {
    backgroundColor: '#E4E6EB',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
  },
  ChatListFriendMessageText: {
    color: '#333',
    fontWeight: '600',
    fontSize: 16,
  },
  ContainerChatMessageFriend: {
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
  },
  ContainerIconOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
