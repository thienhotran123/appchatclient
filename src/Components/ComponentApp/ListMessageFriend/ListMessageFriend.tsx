import {Text, View} from 'react-native';
import React from 'react';
import {ListMessageFriendCss} from './ListMessageFriendCss';
const ListMessageFriend = ({item}: any) => {
  return (
    <View style={ListMessageFriendCss.ChatListFriend}>
      <View style={ListMessageFriendCss.ContainerChatMessageFriend}>
        <View style={ListMessageFriendCss.ChatListFriendMessage}>
          <Text style={ListMessageFriendCss.ChatListFriendMessageText}>
            {item.message}
          </Text>
        </View>
        <View style={ListMessageFriendCss.ContainerIconOption}></View>
      </View>
    </View>
  );
};

export default ListMessageFriend;
