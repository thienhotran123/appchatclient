import {Image, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {scale} from '../../../Util/reponsive';
const ListUserOnline = () => {
  return (
    <View  style={{width: scale(52), height: scale(90) ,marginRight: 20}}>
      <TouchableOpacity
        style={{width: scale(52), height: scale(52)}}>
        <Image
          source={require('../../../Public/image/user.jpg')}
          style={{width: '100%', height: '100%', borderRadius: 50}}
        />
        <View
          style={{
            position: 'absolute',
            width: 16,
            height: 16,
            borderRadius: 50,
            backgroundColor: '#33CC33',
            bottom: 0,
            right: 0,
            borderColor: '#FFFFFF',
            borderWidth: 3,
          }}></View>
      </TouchableOpacity>
      <Text style={{textAlign:'center' ,fontSize:14 ,fontWeight:'600' , color:'#666666'}} numberOfLines={2}>Trần văn thiên</Text>
    </View>
  );
};

export default React.memo(ListUserOnline);
