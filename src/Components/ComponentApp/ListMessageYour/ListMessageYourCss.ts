import { StyleSheet } from 'react-native'

export const ListMessageYourCss = StyleSheet.create({
    ContainerIconOption: {
        flex: 1,
        flexDirection:'row',
        alignItems: 'center',
      },
      ChatListYourSelf:{
        width: '100%',
        marginVertical:4 ,
        justifyContent:'flex-end',
        alignItems:'flex-end'
      },
      ContainerChatMessageYourSelf:{
        flexDirection: 'row',
        width: '90%',
        alignItems: 'center',
      },
      ChatListYourSelfMessage:{
        backgroundColor: '#0065D7',
        padding: 10,
        borderTopLeftRadius:10,
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10
    
      },
      ChatListYourSelfMessageText:{
        color: '#fff',
        fontWeight: '600',
        fontSize: 16,
      }
})