import {Text, View} from 'react-native';
import React from 'react';
import {ListMessageYourCss} from './ListMessageYourCss';
const ListMessageYour = ({item}: any) => {
  return (
    <View style={ListMessageYourCss.ChatListYourSelf}>
      <View style={ListMessageYourCss.ContainerChatMessageYourSelf}>
        <View style={ListMessageYourCss.ContainerIconOption}></View>
        <View style={ListMessageYourCss.ChatListYourSelfMessage}>
          <Text style={ListMessageYourCss.ChatListYourSelfMessageText}>
            {item.message}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default ListMessageYour;
