import {Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {verticalScale} from '../../../Util/reponsive';
interface BtnProps {
  width: number | string;
  height: number;
  TextName: string;
  color: string;
  size: number;
  backgroundColor: string;
}
const ButtonCustomer = ({
  width,
  height,
  TextName,
  color,
  size,
  backgroundColor,
}: BtnProps) => {
  return (
    <TouchableOpacity
      style={{
        width: width,
        height: verticalScale(height),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: backgroundColor,
      }}>
      <Text style={{color: color, fontWeight: '600', fontSize: size}}>
        {TextName}
      </Text>
    </TouchableOpacity>
  );
};

export default ButtonCustomer;
