import React, {useEffect, useMemo, useReducer, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {navigationRef} from '../Util/navigation';
import DefaultLayoutDrawer from './DefaultDrawerScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Chat from '../Screens/ScreenApp/Chat/Chat';
import {SetState} from '../Redux/StateSlice';
import {LoadingDefault} from '../Components/Components';
import {
  ForgotPass,
  HomeScreen,
  Login,
  Register,
  VerificationCode,
} from '../Screens/Screens';
import {useDispatch} from 'react-redux';
const screenOptions = {
  headerShown: false,
};
const Auth = React.createContext({
  Login: (User: any) => {},
});
const Stack = createNativeStackNavigator();
const defaultLayout = () => {
  const [LoadingApp, SetLoading] = useState<boolean>(false);
  const dispatchState = useDispatch();
  const [state, dispatch] = useReducer(
    (prevState: any, action: any) => {
      switch (action.type) {
        case 'Login':
          return {
            token: action.token,
          };
        case 'LoginFail':
          return {
            token: action.token,
          };
      }
    },
    {
      token: null,
    },
  );
  useEffect(() => {
    const fetchLogin = () => {
      SetLoading(true);
      AsyncStorage.getItem('Login')
        .then(data => {
          if (data !== null) {
            const parserData = JSON.parse(data);
            dispatch({type: 'Login', token: 'Login'});
            dispatchState(
              SetState({
                name: parserData.username,
                password: parserData.password,
                image: parserData.avatarImage,
                id: parserData._id,
              }),
            );
            return;
          }
        })
        .catch(err => {
          AsyncStorage.removeItem('Login').finally(() =>
            dispatch({type: 'LoginFail', token: null}),
          );
        })
        .finally(() => SetLoading(false));
    };
    fetchLogin();
  }, []);
  const authContext = useMemo(() => {
    return {
      Login: (User: any) => {
        dispatch({type: 'Login', token: 'User'});
        dispatchState(
          SetState({
            name: User.username,
            password: User.password,
            image: User.avatarImage,
            id: User._id,
          }),
        );
      },
      LogOut: () => {},
    };
  }, []);
  return (
    <Auth.Provider value={authContext}>
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator screenOptions={screenOptions}>
          {LoadingApp ? (
            <Stack.Screen name="Loading" component={LoadingDefault} />
          ) : state?.token == null ? (
            <>
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Register" component={Register} />
              <Stack.Screen name="ForgotPass" component={ForgotPass} />
              <Stack.Screen
                name="VerificationCode"
                component={VerificationCode}
              />
            </>
          ) : (
            <>
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="Chat" component={Chat} />
              <Stack.Screen name="Drawer" component={DefaultLayoutDrawer} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </Auth.Provider>
  );
};
export {Auth};
export default defaultLayout;
