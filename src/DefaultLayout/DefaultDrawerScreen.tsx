import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { SettingDrawer } from '../Screens/Screens';
const DrawerStack = createDrawerNavigator();
const DefaultLayoutDrawer = () => {
  return (
    <DrawerStack.Navigator>
      <DrawerStack.Screen name="Setting" component={SettingDrawer} />
    </DrawerStack.Navigator>
  );
};

export default DefaultLayoutDrawer;
